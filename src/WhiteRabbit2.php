<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        
        $returnArray = array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );
        $a = array(100,50,20,10,5,2,1);

        for ($i=0; $i < 7; $i++) { 
            while($a[$i] <= $amount){
                $amount = $amount - $a[$i];
                $returnArray[$a[$i]] = $returnArray[$a[$i]] + 1;
            }       
            
        }
        return $returnArray;
    }
}

